import java.io.File

object Database {

    var projectPath = "/home/djsushi/projects/IJ/sk-radio-song-scraper/"

    fun getSongs(radioName: String): List<Pair<String, Song>> {
        val songs = mutableListOf<Pair<String, Song>>()
        val fileHandle = File("${projectPath}src/main/resources/radios/${radioName}.csv")
        if (!fileHandle.exists())
            return listOf()

        fileHandle.forEachLine {
            val (time, artist, title) = it.split(";;")
            songs.add(Pair(time, Song(artist, title)))
        }
        return songs
    }

    fun put(radioName: String, time: String, song: Song) {
        val fileHandle = File("${projectPath}src/main/resources/radios/${radioName}.csv")
        if (!fileHandle.exists())
            fileHandle.createNewFile()

        fileHandle.appendText("$time;;${song.artist};;${song.title}\n")
    }

    fun getLast(radioName: String): Pair<String, Song>? {
        if (getSongs(radioName).isEmpty())
            return null
        return getSongs(radioName).last()
    }

}