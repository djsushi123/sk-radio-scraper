import org.jsoup.Jsoup
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import javax.xml.crypto.Data

@ExperimentalStdlibApi
fun main() {

    Database.projectPath = "/home/djsushi/projects/IJ/sk-radio-song-scraper/"

    // scrape the song list from the website and delete all the entries with invalid song data
    val scraped = scrape().filter {
        val song = it.second
        song.artist !in listOf(
            "Informácia o hranej pesničke",
            "Informácie o hranej pesničke",
            "Na tomto rádiu"
        )
    }

    for (radio in scraped) {
        val radioName = radio.first.replace("/", " ")
        val artist = radio.second.artist
        val title = radio.second.title
        val current = LocalDateTime.now()
        val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")


        println(radio)

        if (Database.getLast(radioName) == null)
            Database.put(radioName, current.format(formatter), radio.second)

        if (Database.getLast(radioName)?.second == Song(artist, title))
            continue

        Database.put(radioName, current.format(formatter), radio.second)
    }

}


/**
 * Scrapes the radia.sk website for song, artist and radio data. Returns a *List* containing *Map*s with
 * radio name and song data.
 */
@ExperimentalStdlibApi
fun scrape(): List<Pair<String, Song>> {

    val songList: MutableList<Pair<String, Song>> = mutableListOf()

    // download the HTML
    val doc = Jsoup.connect("https://www.radia.sk/radia").get()

    // get the table
    val table = doc.getElementById("radia_tabulka")

    // get the rows
    val rows = table.children()

    rows.removeLast()

    // for each row
    rows.forEach { row ->
        // get radio name
        val radio = row.child(0).child(1).text()

        // get song data
        val artist = row.child(2).child(1).text()
        val title = row.child(2).child(2).text()
        val song = Song(artist, title)

        songList.add(Pair(radio, song))
    }

    return songList

}